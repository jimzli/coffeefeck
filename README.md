# README #

This is a Brainf*ck interpreter written using CoffeeScript. In addition to the standard 8 op-codes, it has been extended to include debugging through the "#" op-code.

Run using:

```
#!bash

coffee bf.coffee
```

Once running, you refer to the inbuilt help for usage:

```
#!bash

bf> ?
Commands available:12
 ?      help me
 c      continue executing the currently executing program until breakpoint if debug enabled, otherwise until completion
 d      debug toggle to print dumps during execution
 dc     dump the executing code and instruction pointer
 dm     dump mem that resulted from the most recent execution
 dp     dump pointers that resulted from the most recent execution
 i      if no args then dumps input, otherwise buffers for processing during execution
 m      get or sets the uppermem boundary (note: causes a machine reset)
 n      execute the next step of the currently executing program
 q      quit the process
 r      resets the machine state explicitly
 x      execute program until breakpoint if debug enabled, otherwise until completion
```

Worth noting the default memsize:

```
#!bash

bf> m
Mem size is 32768
```

If you prefer, you can compile to JavaScript with source mapping:

```
#!bash

coffee -cm bf.coffee
```

Additional details, including examples, can be found on [Wikipedia](http://en.wikipedia.org/wiki/Brainfuck).