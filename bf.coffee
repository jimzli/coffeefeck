`//# sourceMappingURL=bf.map`
(require "source-map-support").install
rl = require "readline"

class MachineError extends Error
	constructor : (message, code_pos) ->
		@name = "MachineError"
		@message = message
		@pos = code_pos
		Error.captureStackTrace(this, arguments.callee)
		Error.call(this)

class BreakPoint extends Error
	constructor : (code_pos) ->
		@name = "BreakPoint"
		@pos = code_pos
		Error.call(this)

# the bf state machine
class Machine
	code		: ""	# executing code
	inputs		: ""	# inputs to be processed
	flag		: 0		# execution flags. Make this bitwise, currently just 0/1 to indicate breakpoints

	cells		: []	# memory array
	ltr_ptrs	: []	# left brace to right
	rtl_ptrs	: []	# right brace to left
	mem_ptr		: 0		# memory position
	ins_ptr		: 0		# program counter

	instrSet :
		">" : ($) ->
			$.mem_ptr = 0 if ++$.mem_ptr >= $.cells.length
		"<" : ($) ->
			$.mem_ptr = $.cells.length - 1 if --$.mem_ptr < 0
		"+" : ($) ->
			$.cells[$.mem_ptr]++
		"-" : ($) ->
			$.cells[$.mem_ptr]--
		"." : ($) ->
			process.stdout.write String.fromCharCode $.cells[$.mem_ptr]
		"," : ($) ->
			c = $.inputs.shift
			$.cells[$.mem_ptr] = if not c? and c isnt "\n" and c isnt "\r" then c.charCodeAt else 0
		"[" : ($) ->
			$.ins_ptr = $.ltr_ptrs[$.ins_ptr] if $.cells[$.mem_ptr] is 0
		"]" : ($) ->
			$.ins_ptr = $.rtl_ptrs[$.ins_ptr] if $.cells[$.mem_ptr] isnt 0
		"#" : ($) ->
			if ($.flag is 1)
				$.ins_ptr++
				throw new BreakPoint $.ins_ptr
		"\n" : ($) ->
			$.ins_ptr = -1

	constructor : (@cfg) ->
		@reset
	
	# resets points and memory, but will not change ltr or rtl and restore inputs stack
	reset : ->
		@cells = new Uint16Array @cfg.cells_max_size
		@mem_ptr = 0
		@ins_ptr = 0

	read : (code) ->
		brace = 0
		temp = new Uint16Array code.length

		@ltr_ptrs = new Uint16Array code.length
		@rtl_ptrs = new Uint16Array code.length

		for ins_ptr in [0...code.length] by 1
			switch c = code[ins_ptr]
				when "["
					brace++
					temp[brace] = ins_ptr
				when "]"
					left_ptr = temp[brace]
					@rtl_ptrs[ins_ptr] = left_ptr
					@ltr_ptrs[left_ptr] = ins_ptr
					brace--
				
					if brace < 0
						throw new MachineError "Premature end token", ins_ptr
				else
					if not @instrSet[c]?
						throw new MachineError "Unexpected token '#{c}'", ins_ptr
		
		if brace isnt 0
			throw new MachineError (if brace > 0 then "Missing #{brace} end token(s)" else "Unexpected #{-brace} extra start token(s)"), code.length
	
	step : ->
		if @ins_ptr < @code.length and @ins_ptr >= 0
			c = @code[@ins_ptr]

			@instrSet[c] @
			@ins_ptr++

			return true

	continue : ->
		while @step() then continue

	execute : (@code, @inputs, @flag) ->
		@continue()

# simple repel that can act upon the state machine
class Repel
	debug : false
	inputBuffer : new Array

	cfg :
		cells_max_size : 32768

	toggleDebug : ->
		return @debug = not @debug

	constructor : ->
		@machine = new Machine @cfg

	next : (line) ->
		bits = line.split /\ (.+)?/, 2
		cmd = bits[0]
		arg = bits[1]
		action = @cmds[cmd] or @cmds["x"]
		
		# defaulted, so use "cmd" as the "arg"
		if not @cmds[cmd]?
			arg = cmd
		
		action arg, @

	close : ->
		process.exit 0

	cmds :
		"m" : (arg, $) ->
			return "get or sets the uppermem boundary (note: causes a machine reset)" if arg is "?"
			
			if arg?
				$.cfg.cells_max_size = parseInt arg or 32768
				console.log "Changed mem size to #{$.cfg.cells_max_size}"
				$.cmds["r"] arg, $
			else
				console.log "Mem size is #{$.cfg.cells_max_size}"
		"d" : (arg, $) ->
			return "debug toggle to print dumps during execution" if arg is "?"
	
			console.log "Debug #{$.toggleDebug()}"
		"dp" : (arg, $) ->
			return "dump pointers that resulted from the most recent execution" if arg is "?"
			
			$.dumpPointers $.machine.ltr_ptrs, $.machine.rtl_ptrs
		"dm" : (arg, $) ->
			return "dump mem that resulted from the most recent execution" if arg is "?"
			
			$.dumpMemory $.machine.cells, $.machine.mem_ptr
		"dc" : (arg, $) ->
			return "dump the executing code and instruction pointer" if arg is "?"

			$.dumpLine $.machine.code, $.machine.ins_ptr
		"q" : (arg, $) ->
			return "quit the process" if arg is "?"
			
			$.close -1
		"r" : (arg, $) ->
			return "resets the machine state explicitly" if arg is "?"
			
			console.log "Reset machine"
			$.machine.reset()
			$.inputBuffer = new Array
		"?" : (arg, $) ->
			return "help me" if arg is "?"
			
			cmds = (Object.keys $.cmds).sort()
			console.log "Commands available:#{cmds.length}"
			for cmd in cmds
				console.log " #{cmd}\t#{($.cmds[cmd] "?")}"
			return
		"i" : (arg, $) ->
			return "if no args then dumps input, otherwise buffers for processing during execution" if arg is "?"
			
			if arg?
				$.inputBuffer = arg.split ""
				console.log "Stored #{$.inputBuffer.length} byte(s)"
			else
				console.log "'#{$.inputBuffer.join ""}'"
				console.log "Read #{$.inputBuffer.length} bytes(s)"
		"n" : (arg, $) ->
			return "execute the next step of the currently executing program" if arg is "?"
			
			try
				$.machine.step()
			catch e
				if e instanceof BreakPoint
					console.log "\nBreakPoint hit"
					$.dumpLine $.machine.code, e.pos
				else
					console.log "\nMachineError:\n#{e.message}"
					$.dumpLine $.machine.code, e.pos
		"c" : (arg, $) ->
			return "continue executing the currently executing program until breakpoint if debug enabled, otherwise until completion" if arg is "?"
			
			try
				$.machine.continue()
				console.log "" if $.machine.code.indexOf "." > -1
			catch e
				if e instanceof BreakPoint
					console.log "\nBreakPoint hit"
					$.dumpLine $.machine.code, e.pos
				else
					console.log "\nMachineError:\n#{e.message}"
					$.dumpLine $.machine.code, e.pos
		"x" : (arg, $) ->
			return "execute program until breakpoint if debug enabled, otherwise until completion" if arg is "?"
			return if not arg?

			try
				$.machine.reset()
				$.machine.read arg

				$.machine.execute arg, $.inputBuffer.slice(0), +$.debug || 0
				console.log "" if $.machine.code.indexOf "." > -1
			catch e
				if e instanceof BreakPoint
					console.log "\nBreakPoint hit"
					$.dumpLine $.machine.code, e.pos
				else
					console.log "\nMachineError:\n#{e.message}"
					$.dumpLine $.machine.code, e.pos

	dumpLine : (line, pos) ->
		console.log "\nCode:#{pos}\n#{line}"
		if pos > -1 and pos <= line.length
			process.stdout.write " " while pos-- > 0
			console.log "^\n"
		else
			console.log "(out of range)\n"
	
	dumpPointers : (ltr_ptrs, rtl_ptrs) ->
		console.log "\nPointers:"
		process.stdout.write "ltr ["
		for i in [0...ltr_ptrs.length] by 1
			process.stdout.write ", " if i > 0
			process.stdout.write if ltr_ptrs[i]? then ltr_ptrs[i].toString() else "undef"
		console.log "]"
		
		process.stdout.write "rtl ["
		for i in [0...rtl_ptrs.length] by 1
			process.stdout.write ", " if i > 0
			process.stdout.write if rtl_ptrs[i]? then rtl_ptrs[i].toString() else "undef"
		console.log "]\n"
	
	dumpMemory : (cells, mem_ptr) ->
		console.log "\nMemory:#{mem_ptr}"
		process.stdout.write "["
		for i in [0...cells.length] by 1
			process.stdout.write ", " if i > 0
			process.stdout.write cells[i].toString()
		console.log "]"
		
		if mem_ptr < 0 or mem_ptr > cells.length
			console.log "(out of range)\n"
			return

		process.stdout.write " "
		for i in [0...mem_ptr] by 1
			dlen = cells[i].toString().length + 2
			process.stdout.write " " while dlen-- > 0
		console.log "^\n"

# setup and launch the terminal
reader = rl.createInterface process.stdin, process.stdout
reader.setPrompt "bf> "
reader.repel = new Repel
reader.on "line", (line) ->
	@repel.next line
	reader.prompt()
reader.on "close", ->
	@repel.close()

reader.prompt()